import axios from "../../../axios";

export default async function handler(req, res) {

    const { searchId } = req.query;

    console.log(req.query);
    let response = await axios.get(`/gifs/search?q=${searchId}`);

    let data = await response.data;


    res.status(200).json(data);

}