import axios from "../../axios";

export default async function handler(req, res) {
    let response = await axios.get(`/gifs/trending`);

    let data = await response.data;

    res.status(200).json(data);

}