import axios from "../../../axios";

export default async function handler(req, res) {

    const { id } = req.query;


    let response = await axios.get(`/gifs/${id}`);

    let data = await response.data;

    res.status(200).json(data);

}