import { useRouter } from 'next/router';
import React from 'react';

import GloopDetail from '../components/gloop/GloopDetail';

export default function GloopDetailPage() {

    const route = useRouter();
    const { id } = route.query;
    return <div>


        <GloopDetail id={id} />
    </div>;
}
