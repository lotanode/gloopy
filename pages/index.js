import Head from 'next/head'
import Image from 'next/image'
import { Input } from '../components'
import React, { useEffect, useState, useContext } from 'react';

import axios from "axios";
import Gloops from '../components/gloop/Gloops'
import GloopSearch from '../components/gloop/GloopSearch'
import { AppContext } from '../components/Layout';
import Container from '../components/Container';
export default function Home({ }) {

  const { appState, dispatch } = useContext(AppContext);

  const { isSearching, searchQuery } = appState;


  return (
    <div>





      {(isSearching) ?
        <Container title={`Searching for : ${searchQuery}`}>
          <Gloops query={searchQuery} isSearch />
        </Container>
        :
        <Container title={`Trending`}>
          <Gloops query="trending" />
        </Container>

      }








    </div>
  )
}


// export async function getStaticProps() {


// //   let response = await fetch(`/api/trending`);

// //   let data = await response.data;

// //   console.log(data);

// //   return {
// //     props: {
// //       trending: []


// //     }
// //   }
// // }