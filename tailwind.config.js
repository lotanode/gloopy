module.exports = {

  content: [
    './pages/**/*.{html,js}',
    './components/**/*.{html,js}'
  ],
  darkMode: 'class', // or 'media' or 'class'
  theme: {

    extend: {
      fontFamily: {
        'sans': ['Montserrat', 'ui-sans-serif', 'system-ui']

      },
      colors: {

        brand: {
          light: "#a545fb",
          DEFAULT: "#9F34FF",
          dark: "#8e15fb",
          secondary: {

            light: "#24b6af",
            DEFAULT: "#39b2ac",
            dark: "#32a39e"
          }



        }

      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
  ],
}
