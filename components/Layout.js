
import { createContext, useReducer } from 'react';
import Container from './Container';
import GloopSearch from './gloop/GloopSearch';

const initialState = {
    isSearching: false,
    searchQuery: ""
};

export const appActions = {
    START_SEARCH: "START_SEARCH",
    SET_SEARCH_QUERY: "SET_SEARCH_QUERY"
}


const { START_SEARCH, SET_SEARCH_QUERY } = appActions;


function startSearch(value = true) {
    return {
        type: START_SEARCH,
        payload: value
    }
}
function setSearchQuery(value = "") {
    return {
        type: SET_SEARCH_QUERY,
        payload: value
    }
}
const reducer = (state, { type, payload: p }) => {


    switch (type)
    {
        case START_SEARCH:
            return { ...state, isSearching: p }

        case SET_SEARCH_QUERY:
            return { ...state, searchQuery: p }

        default:

            return state;
    }


}


const AppContext = createContext();




export default function Layout({ children }) {

    const [appState, dispatch] = useReducer(reducer, initialState);

    return (
        <AppContext.Provider value={{ appState, dispatch }}>
            <html className="overflow-scroll dark">

                <body className=" md:px-0 dark-mode-bg">
                    <div className="relative m-auto">

                        <div className="relative pb-16 sm:pb-24 lg:pb-32">


                            <main className="">

                                <div className="flex items-center justify-center bg-brand">


                                    <div className="py-4">

                                        <h1 className="flex w-full text-2xl font-bold text-center text-white"> Gloopy</h1>

                                    </div>

                                </div>




                                <Container> <GloopSearch /></Container>



                                {children}
                            </main>



                        </div>
                    </div>

                </body>
            </html>

        </AppContext.Provider>
    )
}


export { AppContext, setSearchQuery, startSearch };
