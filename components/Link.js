import React from 'react';

import Link from 'next/link';

export default function AppLink({ className, children, ...others }) {
    return (
        <Link {...others}>

            <a className={className}>
                {children}
            </a>
        </Link>
    )
}
