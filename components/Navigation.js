// import React, { useContext, useEffect } from 'react'
// import { AppContext, appActions } from './Layout'
// import { Popover, Transition } from '@headlessui/react'
// import { MenuIcon, XIcon } from '@heroicons/react/outline'
// import { Fragment } from 'react'

// // let { SHOW_NAVIGATION } = appActions;
// const navigation = [
//     { name: 'Pricing', href: '/pricing' },
//     { name: 'FAQ', href: '/faq' },
//     { name: 'Blog', href: '/blog' },

// ]



// const cta = [

//     { name: 'Login', href: '/login', className: "btn" },
//     { name: 'Register', href: '/register', className: "btn btn-secondary" },

// ]

// export default function Navigation() {

//     const { appState, dispatch } = useContext(AppContext);



//     useEffect(() => {


//         //dispatch({ type: appActions.SHOW_NAVIGATION, payload: false })

//     }, [])

//     if (!appState.showNavigation)
//     {
//         return null;
//     }
//     return (
//         <div className="pt-3">
//             <Popover>
//                 {({ open }) => (
//                     <>
//                         <nav
//                             className="relative flex items-center justify-between px-4 mx-auto max-w-7xl sm:px-6"
//                             aria-label="Global"
//                         >
//                             <div className="flex items-center flex-1">
//                                 <div className="flex items-center justify-between w-full md:w-auto">
//                                     <a href="/" className="flex items-center ">

//                                         <img
//                                             className="w-auto h-6 pr-1"
//                                             src="/assets/logo.png"
//                                             alt=""
//                                         />

//                                         <span className="px-2 text-2xl font-semibold text-transparent bg-clip-text bg-gradient-to-r from-brand-secondary to-brand">smurly</span>

//                                     </a>
//                                     <div className="flex items-center -mr-2 md:hidden">
//                                         <Popover.Button className="inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
//                                             <span className="sr-only">Open main menu</span>
//                                             <MenuIcon className="w-6 h-6" aria-hidden="true" />
//                                         </Popover.Button>
//                                     </div>
//                                 </div>
//                                 <div className="hidden md:block md:ml-10 md:space-x-10">
//                                     {navigation.map((item) => (
//                                         <a key={item.name} href={item.href} className="font-medium text-gray-500 dark-mode-text">
//                                             {item.name}
//                                         </a>
//                                     ))}
//                                 </div>
//                             </div>
//                             <div className="hidden text-right md:block">
//                                 {cta.map(({ name, className, href }) => (
//                                     <a
//                                         href={href} className
//                                         className={className}
//                                     >
//                                         {name}
//                                     </a>

//                                 ))}

//                             </div>
//                         </nav>

//                         <Transition
//                             show={open}
//                             as={Fragment}
//                             enter="duration-150 ease-out"
//                             enterFrom="opacity-0 scale-95"
//                             enterTo="opacity-100 scale-100"
//                             leave="duration-100 ease-in"
//                             leaveFrom="opacity-100 scale-100"
//                             leaveTo="opacity-0 scale-95"
//                         >
//                             <Popover.Panel
//                                 focus
//                                 static
//                                 className="absolute inset-x-0 top-0 p-2 transition origin-top-right transform md:hidden"
//                             >
//                                 <div className="overflow-hidden bg-white rounded-lg shadow-md dark-mode-bg-light ring-1 ring-black ring-opacity-5">
//                                     <div className="flex items-center justify-between px-5 pt-4">
//                                         <div>
//                                             <img
//                                                 className="w-auto h-8"
//                                                 src="/assets/logo.png"
//                                                 alt=""
//                                             />
//                                         </div>
//                                         <div className="-mr-2">
//                                             <Popover.Button className="inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
//                                                 <span className="sr-only">Close main menu</span>
//                                                 <XIcon className="w-6 h-6" aria-hidden="true" />
//                                             </Popover.Button>
//                                         </div>
//                                     </div>
//                                     <div className="px-2 pt-2 pb-3 space-y-1">
//                                         {navigation.map((item) => (
//                                             <a
//                                                 key={item.name}
//                                                 href={item.href}
//                                                 className="block px-3 py-2 text-base font-medium text-gray-700 rounded-md hover:bg-gray-50"
//                                             >
//                                                 {item.name}
//                                             </a>
//                                         ))}
//                                     </div>
//                                     <a
//                                         href="#"
//                                         className="block w-full px-5 py-3 font-medium text-center text-indigo-600 bg-gray-50 hover:bg-gray-100"
//                                     >
//                                         Log in
//                                     </a>
//                                 </div>
//                             </Popover.Panel>
//                         </Transition>
//                     </>
//                 )}
//             </Popover>
//         </div>
//     )
// }
