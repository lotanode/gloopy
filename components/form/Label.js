import React from 'react'

export default function Label({ children, ...others }) {
    return (
        <label
            {...others}
        >

            {children}

        </label>
    )
}
