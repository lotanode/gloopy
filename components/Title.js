import React from 'react';


export default function Title({ className, children, level = 1, ...others }) {


    let Tag;
    switch (level)
    {
        case 2:

            Tag = h2;

            break;

        default:
            Tag = h1;
    }
    return (


        <Tag className={className}>
            {children}
        </Tag>

    )
}
