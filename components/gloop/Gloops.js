import React, { useEffect, useState } from 'react';

import axios from "axios";
import GloopList from './GloopList';
function Gloops({ query = "", params, isSearch = false }) {

    const [data, setData] = useState([]);
    let endpoint = "/api/search";

    switch (query)
    {

        case "trending":

            endpoint = "/api/trending";

            break;



    }



    if (isSearch)
    {
        endpoint = `/api/search/${query}`;
    }
    useEffect(async () => {




        const response = await axios.get(endpoint);



        setData(response.data.data)

    }, [query])
    return <GloopList data={data} />;
}

export default Gloops;
