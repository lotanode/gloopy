import React from 'react';
import Image from 'next/image';

export default function Gloop({ src = null, ...others }) {
    return <div className="w-96  mt-5 mr-2.5 rounded-lg bg-brand-secondary flex items-center justify-center">
        {src && <Image src={src} className="rounded-lg " {...others} />}</div>;


}
