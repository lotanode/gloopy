import Link from 'next/link';
import React from 'react';
import Gloop from '.';

export default function GloopList({ data = [] }) {
    return <div className="container flex flex-wrap items-center justify-center overflow-scroll">

        {data.length > 0 && data.map(({ images, id }, i) => {
            return <Link href={`/${id}`} key={id}>
                <a>
                    <Gloop src={images.downsized_large.url} width={images.downsized_large.width} height={images.downsized_large.height} />
                </a>
            </Link>
        })
        }

    </div>
}
