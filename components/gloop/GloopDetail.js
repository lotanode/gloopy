import React, { useState, useEffect } from 'react';
import Gloop from '.';
import axios from 'axios';
import Container from '../Container';
import Link from 'next/link';
export default function GloopDetail({ id }) {

    const [data, setData] = useState(null);

    useEffect(async () => {



        async function fetchData() {
            // You can await here
            if (id)
            {

                const response = await axios.get(`/api/gloop/${id}`);


                // let responseData = await response.json();


                setData(response.data.data)


            }

            // ...
        }


        fetchData();




    }, [])


    return data ? <Container childrenClassName="flex items-center justify-center basis-full flex-col " title={data.title}><Gloop src={data.images.downsized_large.url} width={data.images.downsized_large.width} height={data.images.downsized_large.height} />
        <Link href="/"><a className="mt-10">Back</a></Link>
    </Container> : null
}
