import React, { useEffect, useState, useContext } from 'react';
import { Input } from '..';

import { AppContext, setSearchQuery, startSearch } from '../Layout';

function GloopSearch({ }) {

    const [autoSearch, setAutoSearch] = useState(false);

    const { appState, dispatch } = useContext(AppContext)
    const { searchQuery } = appState;
    const handleChange = (e) => {
        e.preventDefault();
        let value = e.target.value;

        dispatch(setSearchQuery(value))

        if (value.length > 2)
        {
            dispatch(startSearch(true))
        } else
        {
            dispatch(startSearch(false))
        }

    }
    useEffect(() => {




        //dispatch(startSearch(true))





    }, [appState])




    return <div className="flex items-center justify-center">


        <form onSubmit={(e) => {
            e.preventDefault();
            dispatch(startSearch(true))
        }

        } className="flex w-full mt-5 space-x-5 md:max-w-4xl">


            <Input
                type="text"
                name="search"

                id="search"
                value={searchQuery}
                onChange={(e) => {

                    handleChange(e)

                }}
                placeholder="Search for a gloop..."
                className="w-full"

            />
            <button className="rounded-full btn btn-primary">
                Gloop
            </button>


        </form>
    </div>
}



export default GloopSearch;
