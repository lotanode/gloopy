import React from 'react';

export default function Container({ title = '', children, className = "", childrenClassName }) {
    return <div className={`container px-5 py-5 m-auto ${className}`}>

        {title != "" && <h2 className="text-xl font-bold text-center basis-full">{title}</h2>}


        <div className={childrenClassName}>
            {children}
        </div>


    </div>;
}
